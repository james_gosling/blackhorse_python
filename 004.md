
# 关键字
在Python中可以使用如下方式查看关键字
```
import keyword
print(keyword.kwlist)
```

# 运算符
Python中的比较运算符有 ==、!=、\>、\<、\>=、\<=  
Python2中不等于可以另外使用`<>`实现  

