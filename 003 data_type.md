
# 数据类型
## 数字类型
* int  
<sup>注：</sup>Python2中int可以细分为int、long，Python3中已不做区分，就是int  

* float  

* boolean  
0为假(False)，非0为真(True)  

* (复数型)complex  
多用于科学计算

## 非数字类型
* 字符串  

* 列表  

* 元组  

* 字典  

## 不同数据类型间的计算
数字型变量可以直接进行算数运算  
boolean类型在计算时，True当作1，False当作0  


